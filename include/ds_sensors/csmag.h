/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 9/9/24.
//

#ifndef DS_SENSORS_CSMAG_H
#define DS_SENSORS_CSMAG_H

#include "ds_base/sensor_base.h"
#include "ds_core_msgs/RawData.h"
#include "ds_sensor_msgs/CsMagnetometer.h"

namespace ds_sensors
{
struct CsMagPrivate;

/// @brief Sensor class for the CS Magnetomer (Gee Maggie)
///
/// Topic                         Message Type                          Description
/// ---------                     ----------------------------          ----------------------------------
/// $name/raw                     ds_core_msgs::RawData                 all raw bytes received from sensor
/// $name/cs_magnetometer   ds_sensor_msgs::CsMagnetometer              successfully parsed messages
class CsMag : public ds_base::SensorBase
{
  DS_DECLARE_PRIVATE(CsMag)

public:
  explicit CsMag();
  CsMag(int argc, char* argv[], const std::string& name);
  ~CsMag() override;
  DS_DISABLE_COPY(CsMag)
  ///@brief Parse a message from the CsMag
  ///
  ///
  /// Returns a std::pair<bool, ds_sensor_msgs::CsMagnetometer>.  The boolean indicates whether parsing was
  /// successful.
  ///
  /// \param bytes    Sequence of bytes received
  /// \return
  static std::pair<bool, ds_sensor_msgs::CsMagnetometer> parse_bytes(const ds_core_msgs::RawData& bytes);


protected:
  void parseReceivedBytes(const ds_core_msgs::RawData& bytes) override;
  void setupPublishers() override;
  void setupConnections() override;
private:
  std::unique_ptr<CsMagPrivate> d_ptr_;
};
}
#endif  // DS_SENSORS_CSMAG_H
