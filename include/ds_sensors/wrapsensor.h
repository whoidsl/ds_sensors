/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 12/11/17.
//

#ifndef DS_SENSOR_WRAPSENSOR_H
#define DS_SENSOR_WRAPSENSOR_H

#include "ds_base/sensor_base.h"
#include "ds_core_msgs/RawData.h"
#include "ds_core_msgs/StringCmd.h"
#include "ds_sensor_msgs/WrapSensor.h"

namespace ds_sensors
{
struct WrapSensorPrivate;

/// @brief Sensor class for the Anderra Oxygen Optode models 4330, 4835, and 4831
///
/// Topic                       Message Type                         Description
/// ---------                   ----------------------------         ----------------------------------
/// $name/raw                   ds_core_msgs::RawData                all raw bytes received from sensor
/// $name/oxygen_concentration  ds_sensor_msgs::OxygenConcentration  successfully parsed messages
class WrapSensor : public ds_base::SensorBase
{
  DS_DECLARE_PRIVATE(WrapSensor)

public:
  explicit WrapSensor();
  WrapSensor(int argc, char* argv[], const std::string& name);
  ~WrapSensor() override;
  DS_DISABLE_COPY(WrapSensor)

  ///@brief Parse a message from the Wrap Sensor
  ///
  ///
  /// Returns a std::pair<bool, ds_msgs::WrapSensor>.  The boolean indicates whether parsing was successful.
  ///
  /// \param bytes    Sequence of bytes received
  /// \return
  std::pair<bool, ds_sensor_msgs::WrapSensor>
  parse_bytes(const ds_core_msgs::RawData& bytes);

protected:
  void setupConnections() override;
  void setupPublishers() override;
  void setupServices() override;
  void parseReceivedBytes(const ds_core_msgs::RawData& bytes) override;
  void on_wrap_in_msg(const ds_core_msgs::RawData& bytes);
  bool query_wraps(const ds_core_msgs::StringCmd::Request& req, ds_core_msgs::StringCmd::Response& resp);

private:
  std::string raw_wrap_string;
  std::unique_ptr<WrapSensorPrivate> d_ptr_;
};

}  // namespace ds_sensors

#endif  // DS_SENSOR_WRAPSENSOR_H
