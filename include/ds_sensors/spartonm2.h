#ifndef DS_SENSORS_SPARTONM2_H
#define DS_SENSORS_SPARTONM2_H

#include "ds_base/sensor_base.h"
#include "ds_core_msgs/RawData.h"
#include "ds_sensor_msgs/Ahrs.h"
#include "std_srvs/SetBool.h"

namespace ds_sensors
{
struct SpartonM2Private;

/// @brief Sensor class for the Applied Physics 1540 Vector Magnetometer
///
/// Topic                         Message Type                          Description
/// ---------                     ----------------------------          ----------------------------------
/// $name/raw                     ds_core_msgs::RawData                 all raw bytes received from sensor
/// $name/ahrs                    ds_sensor_msgs::Ahrs                  successfully parsed messages
class SpartonM2 : public ds_base::SensorBase
{
  DS_DECLARE_PRIVATE(SpartonM2)

public:
  explicit SpartonM2();
  SpartonM2(int argc, char* argv[], const std::string& name);
  ~SpartonM2() override;
  DS_DISABLE_COPY(SpartonM2)
  ///@brief Configure a Sparton M2 AHRS, and then process its output.
  ///
  ///
  /// Returns a std::pair<bool, ds_sensor_msgs::Ahrs>.  The boolean indicates whether parsing was
  /// successful.
  ///
  /// \param bytes    Sequence of bytes received
  /// \return
    static std::pair<bool, ds_sensor_msgs::Ahrs> parse_bytes(const ds_core_msgs::RawData& bytes);
  void setupParameters() override;
  void setupConnections() override;
  void setupPublishers() override;
  void setupServices() override;
  void setup() override;  // Need to do some sensor configuration and tell it to start.
  void configureSparton();
 void SpartonSendCommand(const std::string payload);

protected:
  void parseReceivedBytes(const ds_core_msgs::RawData& bytes) override;
  void parseSolnBytes(const ds_core_msgs::RawData& bytes);
  bool enableAhrsControl(std_srvs::SetBool::Request& req, std_srvs::SetBool::Response& resp);
  
private:
  float latitude;
  float longitude;
  float altitude;
  float day;
  bool ahrscon_enabled_;
  std::unique_ptr<SpartonM2Private> d_ptr_;
};
}

#endif
