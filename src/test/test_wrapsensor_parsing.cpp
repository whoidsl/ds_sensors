/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 04/02/22.
//

#include "ds_sensors/wrapsensor.h"

#include <list>
#include <gtest/gtest.h>

// Test fixture for parsing, needed because we want to call ros::Time::init() before our tests.
class WrapSensorTest : public ::testing::Test
{
public:
  // This method runs ONCE before a text fixture is run (not once-per-test-case)
  static void SetUpTestCase()
  {
    ros::Time::init();
  }
};

// This test case should capture known-bad strings that should fail parsing.
TEST_F(WrapSensorTest, failingParses)
{
  // Our bad strings.  Please add a brief indication why the string is bad if not obvious.
  const auto lines = std::list<std::string>{
    "2018/05/01 01:02:03 1.2 \r\n", // Too few fields
    "2018/05/01 01:02:031.2178.9-391130870\r\n", //Incorrect format
  };

  for (const auto& line : lines)
  {
    // Construct a ByteSequence message
    auto byte_msg = ds_core_msgs::RawData{};
    byte_msg.ds_header.io_time = ros::Time::now();
    byte_msg.data = std::vector<unsigned char>(std::begin(line), std::end(line));

    // Attempt to parse
    ds_sensors::WrapSensor WrapSensor;
    auto result = WrapSensor.parse_bytes(byte_msg);
    EXPECT_FALSE(result.first);
  }
}

TEST_F(WrapSensorTest, validParses)
{
  // Simple function to create a ds_sensors_msg::WrapSensor from arguments
  auto wrap = [](float wraps_counter, float wraps_heading) {

    auto msg = ds_sensor_msgs::WrapSensor{};
    msg.wraps_counter = wraps_counter;
    msg.wraps_heading = wraps_heading;
    return msg;
  };

  // Add new test cases here.  These should pass
  const auto test_pairs = std::list<std::pair<std::string, ds_sensor_msgs::WrapSensor>>{
    { "18/05/01 01:55:06 1.2 182.1 -381 1278 70\r\n",
      wrap(1.2, 182.1) },
    { "18/05/01 01:55:06 12.3 359.4 -381 1278 70\r\n",
      wrap(12.3, 359.4) }
  };

  // Loop through all provided cases
  for (const auto& test_pair : test_pairs)
  {
    auto test_str = test_pair.first;
    auto test_msg = test_pair.second;

    // Construct a ByteSequence message
    auto byte_msg = ds_core_msgs::RawData{};
    auto now = ros::Time::now();
    byte_msg.ds_header.io_time = now;
    byte_msg.data = std::vector<unsigned char>(std::begin(test_str), std::end(test_str));

    auto ok = false;
    auto parsed_msg = ds_sensor_msgs::WrapSensor{};
    ds_sensors::WrapSensor WrapSensor;
    std::tie(ok, parsed_msg) = WrapSensor.parse_bytes(byte_msg);

    // Should have succeeded
    EXPECT_TRUE(ok);

    // All fields should match.
    EXPECT_FLOAT_EQ(now.toSec(), parsed_msg.header.stamp.toSec());
    EXPECT_FLOAT_EQ(test_msg.wraps_counter, parsed_msg.wraps_counter);
    EXPECT_FLOAT_EQ(test_msg.wraps_heading, parsed_msg.wraps_heading);
  }
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
