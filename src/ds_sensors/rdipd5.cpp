/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

// Created by ssuman

#include "ds_sensors/rdipd5.h"

#include <sstream>

using namespace boost::posix_time;
using namespace boost::gregorian;


namespace ds_sensors
{
RdiPd5::RdiPd5() : ds_base::SensorBase()
{
}

RdiPd5::RdiPd5(int argc, char* argv[], const std::string& name)
  : ds_base::SensorBase(argc, argv, name)
{
}

RdiPd5::~RdiPd5() = default;

double RdiPd5::seconds_from_epoch(ptime const& t)
{
  boost::posix_time::ptime const EPOCH(date(1970, 1, 1));
  boost::posix_time::time_duration delta(t - EPOCH);
  return (delta.total_microseconds() / 1000000.0);
}

std::tuple<bool, ds_sensor_msgs::Dvl, ds_sensor_msgs::RdiPD5, ds_sensor_msgs::Ranges3D>
RdiPd5::parse_bytes(const ds_core_msgs::RawData& bytes, double beam_angle, bool phased_array, bool hex_ascii)
{

  bool success = false;
  auto msg = ds_sensor_msgs::Dvl{};
  auto pd5 = ds_sensor_msgs::RdiPD5{};
  auto rng = ds_sensor_msgs::Ranges3D{};

  if (bytes.data.size() < 2)
  {
    // Check for zero or small size
    ROS_ERROR_STREAM("BYTES DATA SIZE LESS THAN 2");
    return std::make_tuple(false, msg, pd5, rng);
  }
  // fill in DVL type field so that subsequent processing can use it
  if (phased_array) {
    pd5.dvl_type = ds_sensor_msgs::RdiPD5::DVL_TYPE_PHASED_ARRAY;
  } else {
    pd5.dvl_type = ds_sensor_msgs::RdiPD5::DVL_TYPE_PISTON;
  }

  // Create parsed pd5 message. Only fails when bad header read,
  // checksum fails, or unknown binary data header.
  // INFO already fed out.
  if (hex_ascii)
    {
      unsigned char buffer[1024];

      // Create a std::string representation of the data
      auto buffer_string = std::string{ std::begin(bytes.data), std::end(bytes.data) };
      
      // Find CR and LF at the end of buffer and erase them if found
      std::size_t found_cr  = buffer_string.find("\r");
      if (found_cr != std::string::npos)
	buffer_string.erase(found_cr);
      std::size_t found_lf  = buffer_string.find("\n");
      if (found_lf != std::string::npos)
	buffer_string.erase(found_lf);
      
      // Convert remaining buffer from hex ascii to binary
      try
	{
	  //ROS_ERROR_STREAM(buffer_string);
	  std::string hash = boost::algorithm::unhex(buffer_string);
	  std::copy(hash.begin(), hash.end(), buffer);
	  //ROS_ERROR_STREAM(buffer);
	}
      catch (...)
	{
	  ROS_ERROR_STREAM("rdipd5: exception unhexing the data stream");
	}
      success = parseHeaderID(buffer, 0, bytes.data.size(), &pd5);
    }
  else
    {
      auto buffer = bytes.data.data();

      success = parseHeaderID(buffer, 0, bytes.data.size(), &pd5);
    }

    //success = parseHeaderID(buffer, 0, bytes.data.size(), &pd5);
  if (success)
  {
    msg_to_dvl(&msg, &pd5, beam_angle, phased_array);  // Create parsed dvl message
    msg_to_rng(&rng, &pd5, beam_angle, phased_array);
  }

  msg.header = bytes.header;
  msg.ds_header.io_time = bytes.ds_header.io_time;
  return std::make_tuple(success, msg, pd5, rng);
}

bool RdiPd5::parseHeaderID(const uint8_t* buffer, uint16_t offset, size_t recv_len, ds_sensor_msgs::RdiPD5* big_msg)
{
  bool ok = true;
  uint32_t headerid = (buffer[offset] << 8) + (buffer[offset + 1] << 0);
  if (headerid == 0x7d01 && offset == 0)
  {
    auto df = reinterpret_cast<const rdipd5_structs::dataframe*>(&buffer[offset]);
    if (df->bytes > recv_len) {
      ROS_ERROR_STREAM("RdiPd5 - Header reports we should have received more bytes than we did, dropping...");
      return false;
    }
    ok = df_to_msg(df, big_msg);
    ROS_INFO_STREAM("RdiPd5 - df_to_msg ok: " << ok);
  }
  else
  {
    ROS_WARN_STREAM("RdiPd5 - Header ID not recognized: " << headerid);
    return false;
  }
  return ok;
}

bool RdiPd5::df_to_msg(const rdipd5_structs::dataframe* df, ds_sensor_msgs::RdiPD5* big_msg)
{
  big_msg->config = df->system_config;
  //big_msg->coord_mode = df->coord_mode; // in pd5 this is not a data field, so hardcoding coord mode given typical usage
  big_msg->coord_mode = ds_sensor_msgs::Dvl::DVL_COORD_INSTRUMENT;
  for (int i = 0; i < 4; i++)
    {
      //big_msg->velocity[i] = df->velocity[i] / 1000.0; // velocity in mm/s
      big_msg->range[i] = df->range[i] / 100.0; // ranges in cm
      big_msg->layer_velocity[i] = df->layer_velocity[i] / 1000.0; // velocity in mm/s

      if (big_msg->range[i] > 0.25)
	big_msg->good_beams += 1;
      big_msg->altitude_sum += big_msg->range[i];
      if (abs(df->velocity[i]) != 32768)
	big_msg->velocity[i] = df->velocity[i] * 0.001;
    }
  big_msg->bottom_status = df->bottom_status; // bitmask - a zero code indicates all correlations are OK
  big_msg->ref_layer_start = df->ref_layer_start / 10.0; // layer distances in dm
  big_msg->ref_layer_end = df->ref_layer_end / 10.0; // layer distances in dm
  big_msg->ref_layer_status = df->ref_layer_status; // bitmask - a zero code indicates all correlations are OK
  big_msg->tofp_hour = df->tofp_hour;
  big_msg->tofp_minute = df->tofp_minute;
  big_msg->tofp_second = df->tofp_second;
  big_msg->tofp_hundreths = df->tofp_hundreths;
  big_msg->bit_results = df->bit_results;
  big_msg->sound_vel = df->sound_vel;
  big_msg->temperature = df->temperature * 0.01;
  big_msg->salinity = df->salinity * 0.001;
  big_msg->depth = df->depth / 10.0; // depth in dm
  big_msg->pitch = df->pitch * 0.01;
  big_msg->roll = df->roll * 0.01;
  big_msg->heading = df->heading * 0.01;
  big_msg->bt_distance_east = df->bt_distance_east / 10.0;
  big_msg->bt_distance_north = df->bt_distance_north / 10.0;
  big_msg->bt_distance_up = df->bt_distance_up / 10.0;
  big_msg->bt_distance_error = df->bt_distance_error / 10.0;
  big_msg->ref_distance_east = df->ref_distance_east / 10.0;
  big_msg->ref_distance_north = df->ref_distance_north / 10.0;
  big_msg->ref_distance_up = df->ref_distance_up / 10.0;
  big_msg->ref_distance_error = df->ref_distance_error / 10.0;

  // dvl_time should be made from monotonic hms from dvl rtc - for basic testing use ros time
  big_msg->dvl_time = ros::Time::now().sec + ros::Time::now().nsec / 1000000000.0;

  big_msg->speed_gnd = sqrt(big_msg->velocity[0] * big_msg->velocity[0] + big_msg->velocity[1] * big_msg->velocity[1]);
  big_msg->course_gnd = atan2(big_msg->velocity[0], big_msg->velocity[1]) * 180.0 / 3.14159;

  return true;

}

void RdiPd5::msg_to_rng(ds_sensor_msgs::Ranges3D* rngdata, ds_sensor_msgs::RdiPD5* big_msg, double beam_angle, bool phased_array)
{
  rngdata->ranges.resize(4);
  double range[4], xy_range[4];

  if (phased_array) {
    rngdata->soundspeed_correction_type = rngdata->SOUNDSPEED_CORRECTION_PHASEDARRAYDVL;
  } else {
    rngdata->soundspeed_correction_type = rngdata->SOUNDSPEED_CORRECTION_NORMAL;
  }

  // Z component for each beam is - the altitude
  for (int i = 0; i < 4; ++i)
  {
    //rngdata->ranges[i].range_quality = big_msg->correlation[i]; // correlation not reported in pd5
    double beam_range = big_msg->range[i];

    if (beam_range == 0)
      rngdata->ranges[i].range_validity = ds_sensor_msgs::Range3D::RANGE_INDETERMINANT;
    else
      rngdata->ranges[i].range_validity = ds_sensor_msgs::Range3D::RANGE_VALID;
    rngdata->ranges[i].range.point.z = -beam_range;
    range[i] = beam_range / cos(beam_angle);
    xy_range[i] = range[i] * sin(beam_angle);
  }

  rngdata->ranges[0].range.point.x = -xy_range[0];
  rngdata->ranges[0].range.point.y = 0.0;
  rngdata->ranges[1].range.point.x = +xy_range[1];
  rngdata->ranges[1].range.point.y = 0.0;
  rngdata->ranges[2].range.point.x = 0.0;
  rngdata->ranges[2].range.point.y = +xy_range[2];
  rngdata->ranges[3].range.point.x = 0.0;
  rngdata->ranges[3].range.point.y = -xy_range[3];

}

void RdiPd5::msg_to_dvl(ds_sensor_msgs::Dvl* dvldata, ds_sensor_msgs::RdiPD5* big_msg, double beam_angle, bool phased_array)
{
  dvldata->dvl_time = big_msg->dvl_time;

  if (phased_array) {
    dvldata->dvl_type = ds_sensor_msgs::Dvl::DVL_TYPE_PHASED_ARRAY;
  } else {
    dvldata->dvl_type = ds_sensor_msgs::Dvl::DVL_TYPE_PISTON;
  }

  dvldata->speed_sound = big_msg->sound_vel;
  dvldata->num_good_beams = big_msg->good_beams;

  for (int i = 0; i < 9; i++) {
    dvldata->velocity_covar[i] = -1;
  }

  dvldata->velocity_mode = dvldata->DVL_MODE_BOTTOM;  // Bottom tracking mode
  //dvldata->coordinate_mode = big_msg->coord_mode; // Not included in pd5, so hardcode given the typical usage
  dvldata->coordinate_mode = dvldata->DVL_COORD_INSTRUMENT;

  for (int i = 0; i < 4; i++)
  {
    dvldata->beam_unit_vec[i].x = 0;
    dvldata->beam_unit_vec[i].y = 0;
    dvldata->beam_unit_vec[i].z = -cos(beam_angle);

    // SS - in PD5 velocity is not to be negated
    dvldata->raw_velocity[i] = big_msg->velocity[i];
    //dvldata->raw_velocity[i] = -big_msg->velocity[i];
    dvldata->raw_velocity_covar[i] = -1;                      // sigma = 0.3 m/s
    
    //dvldata->beam_quality[i] = big_msg->correlation[i];       // correlation not reported in pd5
    dvldata->range[i] = big_msg->range[i] / cos(beam_angle);  // DVL reports ALTITUDE, not RANGE. so send RANGE
    dvldata->range_covar[i] = -1;                             // sigma unknown
  }

  // fill in the horizontal component for each beam in
  // the INSTRUMENT frame
  dvldata->beam_unit_vec[0].x = -sin(beam_angle);
  dvldata->beam_unit_vec[1].x = sin(beam_angle);
  dvldata->beam_unit_vec[2].y = sin(beam_angle);
  dvldata->beam_unit_vec[3].y = -sin(beam_angle);

  if (dvldata->coordinate_mode == dvldata->DVL_COORD_INSTRUMENT)
  {
    dvldata->velocity.x = dvldata->raw_velocity[0];
    dvldata->velocity.y = dvldata->raw_velocity[1];
    dvldata->velocity.z = dvldata->raw_velocity[2];
  }

  dvldata->altitude = big_msg->altitude_sum / big_msg->good_beams;
  dvldata->speed_gnd = big_msg->speed_gnd;
  dvldata->course_gnd = big_msg->course_gnd;

}

bool RdiPd5::checksum(uint16_t length, const uint8_t* buffer)
{
  uint16_t sum = 0x00;
  for (int i = 0; i < length; i++)
  {
    sum += buffer[i];
  }
  auto chksum = reinterpret_cast<const uint16_t*>(&buffer[length]);
  return (sum == *chksum);
}

void RdiPd5::setupConnections()
{
  SensorBase::setupConnections();

}

void RdiPd5::setupPublishers()
{
  SensorBase::setupPublishers();
  auto nh = nodeHandle();
  dvl_pub_ = nh.advertise<ds_sensor_msgs::Dvl>(ros::this_node::getName() + "/dvl", 1);
  pd5_pub_ = nh.advertise<ds_sensor_msgs::RdiPD5>(ros::this_node::getName() + "/pd5", 1);
  ranges_pub_ = nh.advertise<ds_sensor_msgs::Ranges3D>(ros::this_node::getName() + "/ranges", 1);
}

void RdiPd5::setupParameters()
{
  SensorBase::setupParameters();

  beam_angle_ = ros::param::param<double>("~/beam_angle_deg", 30) * M_PI / 180.0;
  phased_array_ = ros::param::param<bool>("~/phased_array", false);
  // hex_ascii is true if the data is in hex ascii format (i.e. Alvin) to operate through
  // a moxa with cr-lf delimter included at the end of the ensemble via a different CF flow control parameter
  // while false if the data is raw binary (i.e. Jason or vehicles that do not send the
  // data through a moxa and use straight serial)
  hex_ascii_ = ros::param::param<bool>("~/hex_ascii", false);
}

///*-----------------------------------------------------------------------------*///
///*   TOP FUNCTIONS: receive incoming raw data, create messages, and publish    *///
///*-----------------------------------------------------------------------------*///
void RdiPd5::parseReceivedBytes(const ds_core_msgs::RawData& bytes)
{
  // Create our result variables.
  auto ok = false;
  auto msg = ds_sensor_msgs::Dvl{};
  auto pd5 = ds_sensor_msgs::RdiPD5{};
  auto rng = ds_sensor_msgs::Ranges3D{};

  std::tie(ok, msg, pd5, rng) = RdiPd5::parse_bytes(bytes, beam_angle_, phased_array_, hex_ascii_);

  ROS_INFO_STREAM("RdiPD5 parse_bytes ok: " << ok);
  if (!ok)
  {
    return;
  }

  // update all timestamps based on our rules
  FILL_SENSOR_HDR(msg, msg.header.stamp, msg.ds_header.io_time);
  FILL_SENSOR_HDR(pd5, msg.header.stamp, msg.ds_header.io_time);
  FILL_SENSOR_HDR(rng, msg.header.stamp, msg.ds_header.io_time);
  msg.header.stamp = msg.ds_header.io_time;
  pd5.header.stamp = msg.ds_header.io_time;
  rng.header.stamp = msg.ds_header.io_time;
  for (int i = 0; i < rng.ranges.size(); ++i)
  {
    rng.ranges[i].range.header.frame_id = frameId();
    rng.ranges[i].range.header.stamp = msg.header.stamp;
  }

  dvl_pub_.publish(msg);
  pd5_pub_.publish(pd5);
  ranges_pub_.publish(rng);
  updateTimestamp("dvl", msg.header.stamp);
  updateTimestamp("pd5", msg.header.stamp);
  updateTimestamp("rng", msg.header.stamp);
}

}  // end namespace ds_sensors
