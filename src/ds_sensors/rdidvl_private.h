/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
// file:  src/ds_sensors/rdidvl_private.h

#ifndef DS_SENSORS_RDIDVL_PRIVATE_H
#define DS_SENSORS_RDIDVL_PRIVATE_H

#include "ds_sensors/rdidvl.h"
#include <queue>

namespace ds_sensors
{
// Our private impl struct subclasses SensorBase::Impl
struct RdiDvlPrivate
{
  ros::Publisher dvl_pub_;
  ros::Publisher dvl_status_pub_;
  ros::Publisher pd0_pub_;
  ros::Publisher ranges_pub_;

  ros::Timer updateStatusTimer;
  ros::Duration status_timer_publish_period = ros::Duration(1.0);
  ds_sensor_msgs::DvlStatus status_msg = ds_sensor_msgs::DvlStatus();

  // beam angle in radians.  Probably 30 deg.
  double beam_angle;

  // whether or not this is a phased array
  bool phased_array;

  // hey did ya get any raw dvl's lately
  ros::Time time_last_raw_dvl;
  // when was the last one of em any good
  ros::Time time_good_parse;
  // well alright so how about if we look back a lil bit

  // yeah just about that far
  ros::Duration window_size;
  // when were they good/bad
  std::list<std::pair<ros::Time, bool>> parses_good;

  unsigned int parses_tried_total;
  unsigned int parses_ok_total;

};
}  // ds_sensors

#endif  // DS_SENSORS_RDIDVL_PRIVATE_H
