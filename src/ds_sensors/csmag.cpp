/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 09/09/24.
//

#include "ds_sensors/csmag.h"
#include "csmag_private.h"

#include <sstream>

namespace ds_sensors
{
CsMag::CsMag() : SensorBase(), d_ptr_(std::unique_ptr<CsMagPrivate>(new CsMagPrivate))
{
}

CsMag::CsMag(int argc, char* argv[], const std::string& name)
  : SensorBase(argc, argv, name), d_ptr_(std::unique_ptr<CsMagPrivate>(new CsMagPrivate))
{
}

CsMag::~CsMag() = default;

std::pair<bool, ds_sensor_msgs::CsMagnetometer> CsMag::parse_bytes(const ds_core_msgs::RawData& bytes)
{
  auto msg = ds_sensor_msgs::CsMagnetometer{};

  // The data channel from the instrument has 9 comma-delimited numbers:
  // 1: PPS counter
  // 2-5: ASCII hex for mag 1
  // 6-9: ASCII hex for mag 2
  // Here's an example: 877,32,32,2e,de,33,e9,9d,42
  // Message units for fields are nT and PPS counter is just counts

  std::string mag_nt_vals;

    // Split on white space and pull specific chars
  auto str = std::string{ reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size() };
  auto buf = std::istringstream{ str };
  std::vector<std::string> tokens;
  std::string token;
  if (str.size() < 25 || str.size() > 35) {
	  ROS_ERROR_STREAM("CSMAG: Incorrect buffer size");
	  return { false, msg};
  }
  while (std::getline(buf, token, ',')) {
    tokens.push_back(token);
  }

  // Valid strings are PPS followed by 2 hex ascii words, so length of 9
  if (tokens.size() !=9) {
    ROS_ERROR_STREAM("Failed to parse CSMAG msg, # tokens (expected 9): "<<tokens.size());
    return { false, msg};
  }
  else {
  msg.pps_counts = std::stod(tokens[0]);

  // First make a raw mag string
  std::stringstream raw_csmag_ss;
  raw_csmag_ss << "CSMAG " << ros::Time::now() << " "<<buf.str();
  //msg.raw_mag_string = raw_csmag_ss.str() + "\r\n";
  msg.raw_mag_string = raw_csmag_ss.str();
  // Reassemble tokens into hexadecimal words and then convert to ascii
  std::stringstream ss_mag_1;
  std::stringstream ss_mag_2;
  auto nt_multiplier = 5e-5;
  ss_mag_1 << tokens[1] << tokens[2] << tokens[3] << tokens[4];
  ss_mag_2 << tokens[5] << tokens[6] << tokens[7] << tokens[8];
  unsigned int mag_1 = std::stoul(ss_mag_1.str(), nullptr, 16);
  unsigned int mag_2 = std::stoul(ss_mag_2.str(), nullptr, 16);
  msg.mag_nt_sensor_1 = mag_1 * nt_multiplier;
  msg.mag_nt_sensor_2 = mag_2 * nt_multiplier;
  
  //ROS_WARN_STREAM("msg pps counts: "<<msg.pps_counts);
  //ROS_WARN_STREAM("msg raw mag string: "<<msg.raw_mag_string);
  //ROS_WARN_STREAM("msg mag_nt_sensor_1: "<<msg.mag_nt_sensor_1);
  //ROS_WARN_STREAM("msg mag_nt_sensor_2: "<<msg.mag_nt_sensor_2);

  msg.ds_header = bytes.ds_header;
  msg.header.stamp = msg.ds_header.io_time;

  return { true, msg };
  }
}

void CsMag::setupConnections()
{
  SensorBase::setupConnections();
  DS_D(CsMag);
  auto nh = nodeHandle();
  d->m_csmag_raw_conn_ = addConnection("csmag_raw_connection", boost::bind(&CsMag::parseReceivedBytes, this, _1));

}
void CsMag::setupPublishers()
{
  SensorBase::setupPublishers();
  DS_D(CsMag);
  d->cs_mag_pub_ = nodeHandle().advertise<ds_sensor_msgs::CsMagnetometer>(
      ros::this_node::getName() + "/csmag_field", 10);
}

void CsMag::parseReceivedBytes(const ds_core_msgs::RawData& bytes)
{
  auto ok = false;
  auto msg = ds_sensor_msgs::CsMagnetometer{};

  std::tie(ok, msg) = CsMag::parse_bytes(bytes);

  if (!ok)
  {
    return;
  }

  FILL_SENSOR_HDR_IOTIME(msg, msg.ds_header.io_time);

  DS_D(CsMag);
  d->m_csmag_raw_conn_->send(msg.raw_mag_string);
  d->cs_mag_pub_.publish(msg);
  updateTimestamp("cs_magnetometer", msg.header.stamp);
}
}
