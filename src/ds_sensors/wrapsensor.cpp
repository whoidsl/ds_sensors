/**
 * Copyright 2018 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by ivandor on 04/02/22.
//

#include "ds_sensors/wrapsensor.h"
#include "wrapsensor_private.h"

namespace ds_sensors
{
WrapSensor::WrapSensor() : SensorBase(), d_ptr_(std::unique_ptr<WrapSensorPrivate>(new WrapSensorPrivate))
{
}

WrapSensor::WrapSensor(int argc, char* argv[], const std::string& name)
  : SensorBase(argc, argv, name), d_ptr_(std::unique_ptr<WrapSensorPrivate>(new WrapSensorPrivate))
{
}

WrapSensor::~WrapSensor() = default;

std::pair<bool, ds_sensor_msgs::WrapSensor> WrapSensor::parse_bytes(const ds_core_msgs::RawData& bytes)
{
  auto msg = ds_sensor_msgs::WrapSensor{};

  // Split on white space and pull specific indexes
  auto str = std::string{ reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size() };
  auto buf = std::istringstream{ str };
  const auto fields =
      std::vector<std::string>{ std::istream_iterator<std::string>(buf), std::istream_iterator<std::string>() };

  auto wrap_os = std::ostringstream{};
  wrap_os << "JWS"
          << " " << str << "\n";
  raw_wrap_string = wrap_os.str();
  auto ok = true;

  //
  // Our float parsing function.
  //
  auto parse_float = [&ok](const std::string& field) -> float {
    const auto c_str = field.c_str();
    // Our expected pointer position after parsing is at the very end of the field.
    const auto expected_end = c_str + field.size();
    auto end = static_cast<char*>(nullptr);

    auto result = std::strtof(c_str, &end);
    // Update our 'ok' parameter.  One failure fails everything.
    ok &= (end == expected_end);
    return result;
  };
  
  if (fields.size() < 3) {
	  return { false, {}};
  }

  //ROS_WARN_STREAM("fields 0: "<<fields.at(0));
  //ROS_WARN_STREAM("fields 1: "<<fields.at(1));
  //ROS_WARN_STREAM("fields 2: "<<fields.at(2));

  if (fields.size() > 7) {
	msg.wraps_date = fields.at(1);
	msg.wraps_time = fields.at(2);
  	msg.wraps_counter = parse_float(fields.at(3));
	msg.wraps_heading = parse_float(fields.at(4));
  }
  else if (fields.size() > 3) {
	  msg.wraps_date = fields.at(0);
	  msg.wraps_time = fields.at(1);
	  msg.wraps_counter = parse_float(fields.at(2));
	  msg.wraps_counter = parse_float(fields.at(3));
  }
  if (!ok)
  {
    ROS_WARN_STREAM("Failed to parse " << str);
    return { false, {} };
  }

  msg.ds_header = bytes.ds_header;
  msg.header.stamp = msg.ds_header.io_time;
  return { true, msg };
}

void WrapSensor::setupConnections()
{
  DsProcess::setupConnections();
  DS_D(WrapSensor);
  auto nh = nodeHandle();
  d->m_wrap_status_conn_ = addConnection("wrap_status_connection", boost::bind(&WrapSensor::parseReceivedBytes, this, _1));
  d->m_wrap_usbl_conn_ = addConnection("wrap_usbl_connection", boost::bind(&WrapSensor::parseReceivedBytes, this, _1));
}
void WrapSensor::setupPublishers()
{
  DsProcess::setupPublishers();
  DS_D(WrapSensor);
  auto nh = nodeHandle();
  d->wrap_sensor_pub_ = nh.advertise<ds_sensor_msgs::WrapSensor>(ros::this_node::getName() + "/wrap_status", 10);
};

void WrapSensor::setupServices()
{
  DsProcess::setupServices();
  DS_D(WrapSensor);
  auto nh = nodeHandle();
  d->wrap_sensor_query_srv_ = nh.advertiseService<ds_core_msgs::StringCmd::Request, ds_core_msgs::StringCmd::Response>(
      ros::this_node::getName() + "/query_wraps", boost::bind(&WrapSensor::query_wraps, this, _1, _2));
}

bool WrapSensor::query_wraps(const ds_core_msgs::StringCmd::Request& req, ds_core_msgs::StringCmd::Response& resp)
{
  DS_D(WrapSensor);
  ds_core_msgs::StringCmd srv;
  auto wrap_query_os = std::ostringstream{};
  wrap_query_os << "sms:"<<req.cmd<<",a0,R0|READ\r\n";
  auto query_string = wrap_query_os.str();
  d->m_wrap_usbl_conn_->send(query_string);
  resp.msg = "Sending query to wrap sensor";
  resp.success = true;
  query_string.clear();
  return true;
}

void WrapSensor::parseReceivedBytes(const ds_core_msgs::RawData& bytes)
{
  auto ok = false;
  auto msg = ds_sensor_msgs::WrapSensor{};

  std::tie(ok, msg) = WrapSensor::parse_bytes(bytes);

  if (!ok)
  {
    return;
  }

  FILL_SENSOR_HDR_IOTIME(msg, bytes.ds_header.io_time);

  DS_D(WrapSensor);
  d->m_wrap_status_conn_->send(raw_wrap_string);
  d->wrap_sensor_pub_.publish(msg);
  updateTimestamp("wrap_sensor", msg.header.stamp);
}

}  // namespace ds_sensors
